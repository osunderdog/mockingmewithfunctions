#  Copyright (c) 2020.  This work by Bruce W. Lowther is licensed under CC BY 4.0

import logging
import unittest
from unittest.mock import patch

# import classes under test
from DatabaseInterface import DBConnectionInfo, MyDatabaseAwareClass

# set logging level for testing
logging.basicConfig(level=logging.DEBUG)


# References
# https://docs.python.org/3.8/library/unittest.mock.html

class MyTracingTestExample(unittest.TestCase):
    def setUp(self) -> None:
        self.password = 'FluffyBu77ies'
        self.ci = DBConnectionInfo(dbdriver='/usr/local/lib/libtdsodbc.so',
                                   server='deepthought',
                                   port='1433',
                                   username='SA',
                                   password=self.password)

    def test_MyDatabaseAwareClass(self):
        """example of test that does not monitor logging.  This will log normally."""

        # verify that the string representation of the DBConnectionInfo has a password placeholder.  Not actual password.
        self.assertNotIn(self.password, str(self.ci))

    def test_real_connection_test(self):
        obj = MyDatabaseAwareClass(self.ci)
        obj._get_odbc_connection()
        result_data_set = obj.get_data()
        logging.info(result_data_set)

    @patch("pyodbc.connect")
    def test_logging_context(self, Mock_Patch):
        """Demonstrate assertLogs context"""

        # create assertLogs context.  While the context is active, logs will be captured into lg.
        with self.assertLogs(level=logging.DEBUG) as lg:
            # every log within this context meeting the level criteria will be captured.
            obj = MyDatabaseAwareClass(self.ci)
            obj._get_odbc_connection()

        # if you know that a term appears in the logging stream, but not necessarily which log line.
        # check them all and if any are true.
        self.assertFalse(any([self.password in entry for entry in lg.output]))

    @patch("pyodbc.connect")
    def test_result_set(self, Mock_Patch):
        """Even without mssql database, test that the result set returned is formatted as expected."""
        mock_database_result_set = [["fee", "fie"], ["foe", "fum"]]
        # use mock object to inject the desired result set that would be returned from database
        Mock_Patch.return_value \
            .cursor.return_value \
            .__enter__.return_value \
            .fetchall.return_value \
            = mock_database_result_set

        # have to follow through several levels of pyodbc connection stack to set this.

        # every log within this context meeting the level criteria will be captured.
        obj = MyDatabaseAwareClass(self.ci)
        result_data_set = obj.get_data()
        self.assertListEqual(mock_database_result_set, result_data_set)

        # to see the call stack that was executed by the pyodbc.connect patch
        logging.debug(Mock_Patch.mock_calls)


if __name__ == '__main__':
    unittest.main()
