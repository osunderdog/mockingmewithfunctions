#  Copyright (c) 2020.  This work by Bruce W. Lowther is licensed under CC BY 4.0

import logging

import pyodbc


class DBConnectionInfo:
    ODBC_CONNECTION_STRING_TEMPLATE = "DRIVER={dbdriver};SERVER={server};UID={username};PWD={password}"

    def __init__(self, dbdriver, server, port, username, password):
        self.dbdriver = dbdriver
        self.server = server
        self.port = port
        self.username = username
        self.password = password

    def get_connection_string(self) -> str:
        return self.ODBC_CONNECTION_STRING_TEMPLATE.format(dbdriver=self.dbdriver,
                                                           server=self.server,
                                                           username=self.username,
                                                           password=self.password)

    def __repr__(self) -> str:
        return self.ODBC_CONNECTION_STRING_TEMPLATE.format(dbdriver=self.dbdriver,
                                                           server=self.server,
                                                           username=self.username,
                                                           password="{password}")


class MyDatabaseAwareClass:
    def __init__(self, credential_info: DBConnectionInfo):
        logging.debug("instance created")
        self.credential_info = credential_info

    def _get_odbc_connection(self) -> pyodbc.Connection:
        logging.debug("connecting to the database")
        logging.debug("Connection string:{}".format(self.credential_info))
        return pyodbc.connect(self.credential_info.get_connection_string())

    def get_data(self):
        con = self._get_odbc_connection()
        with con.cursor() as cur:
            cur.execute("select 1 as some_real_complex_sql")
            result = cur.fetchall()
        return result
